package com.example.projectrika

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ProjectActivity : AppCompatActivity() {

    lateinit var projectview: RecyclerView
    lateinit var projectAdapter: ProjectAdapter
    val list = ArrayList<ProjectData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_project)

       projectview = findViewById(R.id.rvProject)
        projectview.layoutManager = LinearLayoutManager(this)

        list.add(
            ProjectData("Duplicate Application", "Project Duplicate Application",
            "https://gitlab.com/rikaaulia1/duplicate-application")
        )

        list.add(
            ProjectData("Profil Guru", "Project Profil Guru",
                "https://gitlab.com/rikaaulia1/profil-guru")
        )

        projectAdapter = ProjectAdapter(list)
        projectview.adapter = projectAdapter

    }
}