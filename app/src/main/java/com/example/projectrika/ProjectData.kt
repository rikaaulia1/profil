package com.example.projectrika

data class ProjectData (
    val judul: String?,
    val desc: String?,
    val url: String
        )